# OTPQRC

![license](https://img.shields.io/badge/license-CC_BY--NC--SA-green.svg)

## Install

```bash
yarn add otpqrc
```

## Usage

**Initialize the library**

```javascript
import OTPQRC from './otpqrc';
const otp = new OTPQRC('AwesomeApp'); // define
```

**Link to Express instance**

```javascript
otp.express(app, '/qrcode'); // expose qrcode 3min (first start)
// Override: OTPQRC.APP_LACCESS = 3 * 60 * 1000;
```

**Get token**

```javascript
otp.getToken();
```

**Show QRCODE in terminal**

```javascript
otp.showQrcode();
```

## Authors

-   Yarflam - _initial worker_

## License

The project is licensed under Creative Commons (BY-NC-SA).