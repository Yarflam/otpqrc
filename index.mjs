import { authenticator } from 'otplib';
import base32 from 'thirty-two';
import qrcode from 'qrcode-terminal';
import crypto from 'crypto';
import path from 'path';
import fs from 'fs';

class OTPQRC {
    static FILE_EXT = 'otp';
    static APP_USER = 'app';
    static APP_NAME = 'OTPQRC';
    static APP_LACCESS = 3 * 60 * 1000; // 3min
    static APP_ENDPOINT = '/otpaccess';

    _target = null;
    _appName = null;
    _appUser = null;
    _limitAccess = 0;
    _secret = crypto.randomBytes(16).toString('hex');

    constructor(appName, dirpath='.', dirname='secure') {
        this._appName = appName || OTPQRC.APP_NAME;
        this._target = path.resolve(dirpath, `${dirname}.${OTPQRC.FILE_EXT}`);
        this.__init();
    }

    __init() {
        /* Get the secret */
        if(fs.existsSync(this._target)) {
            const secret = fs.readFileSync(this._target, 'utf-8');
            if(/^[\w]{16,}$/.test(secret)) {
                this._secret = secret;
                return true;
            }
        }
        /* Save the new secret */
        fs.writeFileSync(this._target, this._secret);
        this._limitAccess = new Date().getTime() + OTPQRC.APP_LACCESS;
        return true;
    }

    __getAccess() {
        return `otpauth://totp/${this._appName}:${
            this._appUser || OTPQRC.APP_USER
        }?secret=${
            base32.encode(this._secret)
        }&issuer=${this._appName}`;
    }

    getToken() {
        return authenticator.generate(base32.encode(this._secret));
    }

    showQrcode(callback) {
        qrcode.generate(
            this.__getAccess(),
            { small: true },
            (qrcode) => {
                if(typeof callback === 'function') return callback(qrcode);
                console.log(qrcode);
            }
        );
        return this;
    }

    express(app, endpoint=OTPQRC.APP_ENDPOINT) {
        if(!this._limitAccess) return this;
        app.get(endpoint, (req, res, next) => {
            if(new Date().getTime() > this._limitAccess) return next();
            /* Generate QRCODE */
            qrcode.generate(
                this.__getAccess(),
                { small: true },
                (qrcode) => {
                    res.writeHead(200, { 'Content-Type': 'text/plain; charset=utf-8' });
                    res.write(`${qrcode}\ncurrent token: ${this.getToken()}`);
                    res.end();
                }
            );
        });
        return this;
    }

    setAppUser(user) {
        this._appUser = user;
        return this;
    }
}

export default OTPQRC;