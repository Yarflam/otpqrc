import OTPQRC from './index.mjs';
import express from 'express';
import http from 'http';

const HOST = '0.0.0.0';
const PORT = 3000;

/* Usage */
const app = express();
const otp = new OTPQRC('mytest'); // define
otp.express(app, '/qrcode'); // expose qrcode

app.get(/^\/?(.+)$/, (req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/plain; charset=utf-8' });
    res.write('Test server working');
    res.end();
});

const server = http.Server(app);
server.listen(PORT, HOST, () => {
    console.log(`The server working on http://127.0.0.1:${PORT}.`);
});